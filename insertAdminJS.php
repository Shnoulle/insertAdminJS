<?php

/**
 * insertAdminJS Plugin for LimeSurvey 2.50 (build 160425 and up)
 *
 * @author Tony Partner <http://partnersurveys.com>
 * @author Denis Chenu <http://sondages.pro>

 * @copyright 2016-2017 Tony Partner <http://partnersurveys.com>
 * @license AGPL v3 ( GNU AFFERO GENERAL PUBLIC LICENSE )
 * @version 3.0
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with insertAdminJS.  If not, see <http://www.gnu.org/licenses/>.
 */

class insertAdminJS extends PluginBase {

    protected $storage = 'DbStorage';

    static protected $name = 'Insert Custom Admin JS File';
    static protected $description = 'Plugin to insert a custom JavaScript file for the admin interface';

    // Settings
    protected $settings = array(
        'jsFileName' => array(
            'type' => 'string',
            'label' => 'File Name (The default is "custom.js", but change it here if necessary)'
        )
    );

    public function init() {
        // Subscribe to beforeControllerAction : only for web.
        $this->subscribe('beforeControllerAction', 'insertJS');
    }


    // Method(s) that handle events
    public function insertJS() {
        // Control if we are in an admin page, register everywhere even is not needed
        $sController=$this->event->get('controller');
        if($this->event->get('controller')=='admin') {
            if($this->get('jsFileName')) {
                $fileName = $this->get('jsFileName');
                if(substr($fileName, -3) != '.js') {
                    $fileName = $fileName.'.js';
                }
            } else {
                $fileName = 'custom.js';
            }
            App()->getClientScript()->registerScriptFile(rtrim(Yii::app()->getConfig('adminstyleurl'),"/")."/".$fileName);
        }
    }
}
